require 'rubygems'
require 'bundler/setup'
Bundler.setup()
Bundler.require

require_relative "services/valid_words_fetcher_service.rb"
require_relative "services/gre_word_list_sanitizer_service.rb"
require_relative "services/valid_word_verification_service.rb"
require_relative "services/valid_word_verification_initializer_service.rb"
require_relative "services/parallel_executor_service.rb"
require_relative "services/application_launcher_service.rb"

GreWordListSanitizerService.sanitize unless File.exists?("util/sorted_gre_words.txt")
ApplicationLauncherService.launch