class ValidWordVerificationInitializerService
	def self.start
		puts Benchmark.measure {
					 loop do
						 valid_dictionary_words = ValidWordsFetcherService.fetch
						 break if valid_dictionary_words.empty?
						 service = ValidWordVerificationService.new(valid_dictionary_words)
						 valid_combinatory_words = service.process
						 valid_combinatory_words.map{|word| puts word }
					 end
				 }
	end
end