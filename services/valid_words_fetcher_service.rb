# The goal of this class is to fetch a set of 10 valid words from the sorted_gre_words.txt file and return it to the processor

class ValidWordsFetcherService
	class << self
		attr_reader :current_index, :filename, :valid_words_list, :increment_count

		def fetch
			return [] if current_index > valid_words_list.size

			valid_words_subset = ThreadSafe::Array.new

			@mutex.synchronize do
				valid_words_subset = next_10_words
				@current_index += 10
			end

			valid_words_subset
		end

		def next_10_words
			start_index = current_index
			end_index = determine_end_index(start_index)
			return [] if start_index > end_index

			if self.valid_words_list[start_index..end_index].nil?
				puts "Start index: #{start_index}"
				puts "end index: #{end_index}"
			end

			self.valid_words_list[start_index..end_index]
		end

		def determine_end_index(start_index)
			(valid_words_list.size > (start_index + self.increment_count)) ? (start_index + self.increment_count - 1) : (valid_words_list.size - 1)
		end

		def initialize_fetcher(filename = 'util/sorted_gre_words.txt', increment_count = 10)
			@current_index = 0
			@increment_count = 10
			@mutex = Mutex.new
			fetch_valid_words_list(filename)
		end

		def fetch_valid_words_list(filename)
			@valid_words_list = []

			File.open(filename).each do |line|
				@valid_words_list.push(line.downcase.strip)
			end
		end
	end
end