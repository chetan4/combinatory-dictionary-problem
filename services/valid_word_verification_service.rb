class ValidWordVerificationService
	attr_reader :combinatory_word_list, :valid_word_list

	def initialize(valid_words_list = [], filename = 'util/combinatory_words.txt')
		@valid_word_list = valid_words_list
		@combinatory_word_list = fetch_combinatory_words(filename)
	end

	# select words where the gsubbing returns a blank string	
	def find_combinatory_words_that_exists_in_dictionary
		words_found_in_dictionary = valid_word_list.select do |	valid_word|
			found_in_dictionary?(valid_word)
		end
	end

	def found_in_dictionary?(valid_word)
		regex = generate_combinatory_word_regex
		valid_word.gsub(regex, "").strip.empty?
	end

	def generate_combinatory_word_regex
		regex_string = self.combinatory_word_list.map {|word| "(#{word})"}.join("|")
		Regexp.new(regex_string)
	end

	def process
		find_combinatory_words_that_exists_in_dictionary
	end

	def fetch_combinatory_words(filename)
		words_list = []

		File.open(filename).each do |word|
			words_list.push(word.downcase.strip)
		end

		words_list
	end
end


# loop_counter = 0
# loop do 
# 	puts " --------- Looping: #{loop_counter} -------------------------"
# 	valid_words = ValidWordsFetcherService.fetch
# 	break if valid_words.empty?
# 	vwfs = ValidWordVerificationService.new(valid_words)

# 	puts vwfs.process
# 	puts loop_counter += 1
# end