class ApplicationLauncherService
	def self.launch
		loop do
			ValidWordsFetcherService.initialize_fetcher
			puts "Launch the app in multi threaded mode or single threaded mode? (1: threaded \n 2: single threaded)"
			mode = gets.strip

			if mode.to_i == 1
				ParallelExecutorService.perform
			elsif mode.to_i == 2
				ValidWordVerificationInitializerService.start
			else
				puts "Bye bye ..:("
				break
			end
		end
	end
end