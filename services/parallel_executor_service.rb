require 'benchmark'

class ParallelExecutorService
	def self.perform
		puts "Running in threaded mode..."
		puts Benchmark.measure {
			threads = []

			3.times do
				threads << Thread.new do
					ValidWordVerificationInitializerService.start
				end
			end

			threads.map(&:join)
		}
	end
end