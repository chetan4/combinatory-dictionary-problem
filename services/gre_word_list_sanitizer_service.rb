class GreWordListSanitizerService
	attr_reader :sanitized_word_list, :source_filename, :filename

	def self.sanitize
		sanitizer = GreWordListSanitizerService.new
		sanitizer.parse_and_select_words
		sanitizer.sort_sanitized_words!
		sanitizer.write_to_file
	end

	def initialize(filename = "util/sorted_gre_words.txt", source_filename = 'util/gre_word_list_and_meanings.txt')
		@sanitized_word_list = []
		@filename = File.join(__dir__, "..", "#{filename}")
		@source_filename = File.join(__dir__, "..", "#{source_filename}")
	end

	def parse_and_select_words
		File.open("#{self.source_filename}").each do |line|
			word = line.split.first
			@sanitized_word_list.push(word) unless word.include?("http://www.graduateshotline.com")
		end
	end

	def sort_sanitized_words!
		@sanitized_word_list.tap { |list|
			list.sort!
			list.uniq!
		}
	end

	def write_to_file
		File.open("#{filename}", "w") do |file|
			sanitized_word_list.each do |word|
				file.puts(word)
			end
		end
	end
end	




