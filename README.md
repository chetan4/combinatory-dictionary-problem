# Combinatory Dictionary

### Assumptions
 * A list of valid dictionary words 
 * This list at the moment is populated by a list of GRE words available freely at http://www.graduateshotline.com
 * Application built as services so that components can be replaced to modify functionality
 * Concurrency introduced by threads at the moment but can be introduced via processes if a shared datastore is used instead of the `valid_words_fetcher.rb`


### Approach

* The list of valid dictionary words is served in groups of 10 to the processor (`valid_words_verification_service.rb`)by the `valid_words_fetcher_service.rb`
* The processor generates a regex using the words provided in the `combinatory_words.txt`to verify if the any combination of the words can used to replace an entire dictionary word. If it can be replaced entirely the word is returned as a valid dictionary word formed by combining multiple words.
* Groups of 10 are used to allow the work to be done in parallel and avoid memory bloat in case of large datasets.
* However, the dictionary itself is loaded in memory in this case. It would make more sense if the dictionary was stored in a persistent disk store.



### To Run the application

* clone the application to your local machine using `git clone git@github.com:idyllicsoftware/combinatory-dictionary-problem.git`
* `bundle install`
* `ruby combinatory_dictionary.rb`
* The application will in 2 modes - 1. Threaded and 2. Single threaded mode. It'll also display some pointless benchmarks along with the result.
* Shows better performance when run on jruby-1.7.13 with threads.




